#!/usr/bin/env groovy
node {
    def branchOrTagName = BRANCH_NAME
    def publishContent = ''
    def version = 'master.latest'
    def project = 'codo-admin'
    def action = ''
    def ssh = ''
    def targetInternalHosts = ''
    def buildNotifyList = 'hongyang.mi@traxretail.com'
    def publishNotifyList = 'hongyang.mi@traxretail.com'
    def from_user = 'jenkins@lenztechretail.com'

    String[] branchArr = branchOrTagName.split("/")

    if (branchArr.length > 0) {
        branchOrTagName = branchArr[branchArr.length-1]
    }

    try {

        stage('Prepare Env') {
            echo '请输入环境参数......'
            checkout scm
            version = branchOrTagName + ".latest"
            def vars = input message: '请输入部署的环境参数', parameters: [
                    text(defaultValue: '', description: '请输入这次部署更新的内容', name: 'publishContent'),
                    text(defaultValue: '8.141.145.53', description: '请输入这次部署机器', name: 'hostname'),
                    text(defaultValue: "${version}", description: '请输入这次部署使用的镜像版本', name: 'version'),
                    choice(choices: ['deploy'], description: '请选择本次操作', name: 'action')
            ]

            publishContent = vars['publishContent']
            hostname = vars['hostname']
            version = vars['version']
            action = vars['action']
            print('本次发布镜像版本:' + version)
            print('本次发布动作:' + action + '服务')
            print('本次使用分支:' + branchOrTagName)
            echo 'branchOrTagName......${branchOrTagName}'

        }

        stage('Deploy') {
            echo '发布......'
            if (currentBuild.result == null || currentBuild.result == 'SUCCESS') {
                        ssh = "ssh -o StrictHostKeyChecking=no -l root  ${hostname} -p 7721"
                        sh "${ssh} 'mkdir /data/project/${project} -p '"
                        sh "scp -P7721 ./docker-compose.yml root@${hostname}:/data/project/${project}"
                        sh "scp -P7721 ./deploy.sh root@${hostname}:/data/project/${project}/"
                        sh "${ssh} 'cd /data/project/${project};sh deploy.sh'"
                        echo "构建完成"
                    }
                }


        stage('Clean WS') {
            //cleanWs cleanWhenFailure: false, cleanWhenUnstable: false, deleteDirs: true, notFailBuild: true
        }

    } catch (e) {
        print('CI/CD构建异常,发送错误邮件给' + buildNotifyList)
        mail to: buildNotifyList,
                from: "${from_user}",
                subject: "【失败】api-service接口[api-service] CI构建通知",
                body: "api-service 构建失败，请前往${BUILD_URL} 查看" +
                        "\n 分支：${branchOrTagName}" +
                        "\n 失败原因：${e.toString()}" +
                        "\n 主要变更如下:" + getChangeLogs()
        throw e
    }
}

@NonCPS
def getChangeLogs() {
    String logs = ""
    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            logs += "\n ${entry.author}: ${entry.msg} [time:${new Date(entry.timestamp)},commitId:${entry.commitId}]"
            def files = new ArrayList(entry.affectedFiles)
            for (int k = 0; k < files.size(); k++) {
                def file = files[k]
                logs += "\n\t${file.editType.name}=> ${file.path}"
            }
        }
    }
    return logs
}